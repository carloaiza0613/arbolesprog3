/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolBinarioB;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.modelo.NodoABB;
import com.arboles.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private ArbolBinarioB arbol = new ArbolBinarioB();
    private Infante infante= new Infante();
    private List<Localidad> localidades;
    
    private boolean verRegistrar;
    private DefaultDiagramModel model;
    
    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }
    
    
    
    @PostConstruct
    public void inicializar()
    {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));
        
        verRegistrar=false;
        
        try {
            arbol.adicionarNodo(new Infante("3", (byte) 1, 
                    "Santiaguito",localidades.get(1)));
            arbol.adicionarNodo(new Infante("2", (byte) 3, 
                    "Sebitas",localidades.get(1)));
            arbol.adicionarNodo(new Infante("8", (byte) 2, 
                    "Mateito",localidades.get(1)));
            arbol.adicionarNodo(new Infante("6", (byte) 3, 
                    "Lucy",localidades.get(1)));
            arbol.adicionarNodo(new Infante("5", (byte) 2, 
                    "Diego",localidades.get(1)));
            arbol.adicionarNodo(new Infante("7", (byte) 2, 
                    "Jose",localidades.get(1)));
            arbol.adicionarNodo(new Infante("4", (byte) 3, 
                    "JK",localidades.get(1)));
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
                //Logger.getLogger(ControladorABB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        pintarArbol();
    }

    public ArbolBinarioB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBinarioB arbol) {
        this.arbol = arbol;
    }

    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }
    
    public void habilitarVerRegistrar()
    {
        verRegistrar= true;
    }
    
    public void deshabilitarVerRegistrar()
    {
        verRegistrar = false;
    }
    
    
    public void pintarArbol()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
        
    }
    
    
    

    private void pintarArbol(NodoABB reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            elementHijo.setId(reco.getDato().getNroIdentificacion());
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+5);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+5);
        }
    }
     private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");
         
        return endPoint;
    }
     
    public DiagramModel getModel() {
        return model;
    }
    
    public void guardarInfante()
    {
        try {
            arbol.adicionarNodo(infante);
            infante= new Infante();
            verRegistrar= false;
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    
    
      public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");

        infanteSeleccionado = id.replaceAll("frmInfantesAbb:diagrama-", "");

    }

    private String infanteSeleccionado = "";

    public void eliminarInfanteMenuContext() {
        
        
        if (arbol.borrarInfante(infanteSeleccionado)!=null) {
            pintarArbol();
            JsfUtil.addSuccessMessage("Eliminado con éxito");
        } else {
            JsfUtil.addErrorMessage("No pudo eliminarse, comuníquese con el administrador ");
        }
    }
    
}
