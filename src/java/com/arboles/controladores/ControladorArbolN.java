/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolBinarioB;
import com.arboles.modelo.ArbolN;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.modelo.NodoABB;
import com.arboles.modelo.NodoN;
import com.arboles.utilidades.FacesUtils;
import com.arboles.utilidades.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorArbolN")
@SessionScoped
public class ControladorArbolN implements Serializable{
     private ArbolN arbol = new ArbolN();
    private Infante infante= new Infante();
    private List<Localidad> localidades;
    
    private boolean verRegistrar;
    private DefaultDiagramModel model;
   
     private ControladorABB contAbb=(ControladorABB) FacesUtils.
                     getManagedBean("controladorABB");
    
    public ControladorArbolN() {
    }

    public ArbolN getArbol() {
        return arbol;
    }

    public void setArbol(ArbolN arbol) {
        this.arbol = arbol;
    }

    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }
    
     @PostConstruct
    public void inicializar()
    {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));
        
        verRegistrar=false;
        /*
        try {
            arbol.adicionarNodo(new Infante("5", (byte) 3, 
                    "Santiaguito",localidades.get(1)), null);
            arbol.adicionarNodo(new Infante("4", (byte) 4, 
                    "Sebitas",localidades.get(1)), arbol.getRaiz().getDato());
            arbol.adicionarNodo(new Infante("7", (byte) 3, 
                    "Mateito",localidades.get(1)),new Infante("4", (byte) 4, 
                    "Sebitas",localidades.get(1)));
            arbol.adicionarNodo(new Infante("2", (byte) 3, 
                    "Lucy",localidades.get(1)),arbol.getRaiz().getDato());
            arbol.adicionarNodo(new Infante("3", (byte) 2, 
                    "Carlitos",localidades.get(1)),arbol.getRaiz().getDato());
            
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
                //Logger.getLogger(ControladorABB.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        cargarArbol();
        pintarArbol();
    }
    
 
     public void pintarArbol()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
        
    }
    
    
    

    private void pintarArbol(NodoN reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            //elementHijo.setId(reco.getDato().getNroIdentificacion());
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            for(NodoN hijo: reco.getHijos())
            {
                pintarArbol(hijo,model, elementHijo,x-10,y+5);
                x += 10;                
            }
        }
    }
    

     public void guardarInfante()
    {
//        try {
//            arbol.adicionarNodo(infante);
//            infante= new Infante();
//            verRegistrar= false;
//            pintarArbol();
//        } catch (InfanteExcepcion ex) {
//            JsfUtil.addErrorMessage(ex.getMessage());
//        }
    }
    
    
      public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");

        infanteSeleccionado = id.replaceAll("frmInfantesAbb:diagrama-", "");

    }

    private String infanteSeleccionado = "";

    public void eliminarInfanteMenuContext() {
        
        
//        if (arbol.borrarInfante(infanteSeleccionado)!=null) {
//            pintarArbol();
//            JsfUtil.addSuccessMessage("Eliminado con éxito");
//        } else {
//            JsfUtil.addErrorMessage("No pudo eliminarse, comuníquese con el administrador ");
//        }
    }

    
      public void habilitarVerRegistrar()
    {
        verRegistrar= true;
    }
    
    public void deshabilitarVerRegistrar()
    {
        verRegistrar = false;
    }
    
    public void cargarArbol()
    {
         try {
             //Algoritmo
             arbol.adicionarNodo(new Infante("0", (byte) 0,
                     "Yo",null), null);
            
             
             List<Infante> padres= new ArrayList<Infante>();
             padres.add(arbol.getRaiz().getDato());
             adicionarPreOrden(contAbb.getArbol().getRaiz(),padres,0);
             
             
             
            
            System.out.println("Cantidad: "+contAbb.getArbol().
                     getCantidadNodos());
//             for(Infante inf: contAbb.getArbol().recorrerPreOrden())
//             {
//                 System.out.println(inf.getNombre()+"\n");
//             }
             
         } catch (InfanteExcepcion ex) {
             JsfUtil.addErrorMessage(ex.getMessage());
         }
        
    }
    
    
    private void adicionarPreOrden(NodoABB reco, List<Infante> padres, int contizq) throws InfanteExcepcion {
        if (reco != null) {
            List<Infante> padresNuevos= new ArrayList<>();
            int contPapas=0;
            for(byte i=0;i< reco.getDato().getEdad();i++)
            {    
                Infante infanteNuevo = new Infante(
                        reco.getDato().getNroIdentificacion(), reco.getDato().getEdad(),
                reco.getDato().getNombre(), reco.getDato().getLocalidad());
                infanteNuevo.setCodigo(++contizq);
                if(contPapas>=padres.size())
                {
                    contPapas=0;
                }
                arbol.adicionarNodoxCodigo(infanteNuevo, padres.get(contPapas));
                padresNuevos.add(infanteNuevo);
                contPapas++;
            }
            adicionarPreOrden(reco.getIzquierda(),padresNuevos, contizq);
            contizq=contizq+contAbb.getArbol().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrden(reco.getDerecha(),padresNuevos, contizq);
             //   contizq=contizq+contder;
            /*if(reco.getDerecha()!=null)
            {
               contder=reco.getDerecha().getDato().getEdad();
            }*/
            
//            if(reco.getIzquierda()!=null)
//            {
//                adicionarPreOrden(reco.getIzquierda(),padresNuevos, contizq,contder);  
//                contizq=contizq+reco.getIzquierda().getDato().getEdad();
//            }
//            adicionarPreOrden(reco.getDerecha(),padresNuevos,contizq, contder);
        }
    }
    
}
