/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class NodoABB implements Serializable{
    private Infante dato;
    private NodoABB izquierda;
    private NodoABB derecha;

    public NodoABB(Infante dato) {
        this.dato = dato;
    }

    public Infante getDato() {
        return dato;
    }

    public void setDato(Infante dato) {
        this.dato = dato;
    }

    public NodoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABB derecha) {
        this.derecha = derecha;
    }
    
    public boolean esHoja()
    {
        return izquierda == null && derecha == null;
    }
    
}
