/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import com.arboles.excepciones.InfanteExcepcion;
import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class ArbolN implements Serializable{
    private NodoN raiz;
    private int cantidadNodos;

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }
    
    
    
    public void adicionarNodo(Infante dato, Infante padre) throws InfanteExcepcion
    {
       if(raiz == null)
       {
           raiz = new NodoN(dato);
           
       }
       else
       {
           adicionarNodo(dato, padre, raiz);
           
       }
       cantidadNodos++;
       
    }
    
    public boolean adicionarNodo(Infante dato, Infante padre, NodoN pivote) throws InfanteExcepcion
    {
           // boolean adicionado=false;
          if(pivote.getDato().getNroIdentificacion().
                  compareTo(padre.getNroIdentificacion())==0)
          {
              //Es el padre donde debo adicionar
              pivote.getHijos().add(new NodoN(dato));
              //adicionado=true;
              return true;
          }
          else
          {
              for(NodoN hijo: pivote.getHijos())
              {
                  if(adicionarNodo(dato, padre,hijo))
                      break;
              }
          }    
          return false;
    }
    
    
    public void adicionarNodoxCodigo(Infante dato, Infante padre) throws InfanteExcepcion
    {
       if(raiz == null)
       {
           raiz = new NodoN(dato);
           
       }
       else
       {
           adicionarNodoxCodigo(dato, padre, raiz);
           
       }
       cantidadNodos++;
       
    }
    
    public boolean adicionarNodoxCodigo(Infante dato, Infante padre, NodoN pivote) throws InfanteExcepcion
    {
           // boolean adicionado=false;
          if(pivote.getDato().getCodigo()==padre.getCodigo())
          {
              //Es el padre donde debo adicionar
              pivote.getHijos().add(new NodoN(dato));
              //adicionado=true;
              return true;
          }
          else
          {
              for(NodoN hijo: pivote.getHijos())
              {
                  if(adicionarNodoxCodigo(dato, padre,hijo))
                    break;
                  
              }
          }    
          return false;
    }
    
}
