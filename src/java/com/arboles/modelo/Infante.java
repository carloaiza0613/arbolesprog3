/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class Infante implements Serializable{
    private String nroIdentificacion;
    private byte edad;
    private String nombre;
    private Localidad localidad;
    private int codigo;

    public Infante() {
        localidad= new Localidad();
        edad=1;
    }

       
    public Infante(String nroIdentificacion, byte edad, String nombre, Localidad localidad) {
        this.nroIdentificacion = nroIdentificacion;
        this.edad = edad;
        this.nombre = nombre;
        this.localidad = localidad;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    
    public String getNroIdentificacion() {
        return nroIdentificacion;
    }

    public void setNroIdentificacion(String nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public byte getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    @Override
    public String toString() {
        return  nroIdentificacion+ " "+this.getNombre()+" "+codigo;
    } 
    
    
    
    
}
